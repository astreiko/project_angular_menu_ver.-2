import { Component, OnInit } from '@angular/core';

type Nav = {
  icon: string;
  name: string;
  id: number;
  isActive: boolean;
}
type Item = {
  name: string;
  isDisplay: boolean;
  id: number;
}

@Component({
  selector: 'header-menu',
  templateUrl: './header-app.component.html',
  styleUrls: ['./header-app.component.scss']
})
export class HeaderAppComponent implements OnInit {
  public isDisplay: boolean = true;
  public isShowBlock1: boolean = false;
  public buttonId: number = 1;
  public buttons: Nav[] = [
    { isActive: false, id: 1, icon: './assets/images/svg/user.svg', name: 'Account' },
    { isActive: false, id: 2, icon: './assets/images/svg/folder-password.svg', name: 'Password' },
    { isActive: false, id: 3, icon: './assets/images/svg/mobile-browser.svg', name: 'Mobile Number' },
    { isActive: false, id: 4, icon: './assets/images/svg/email.svg', name: 'Email' },
  ];
  public items: Item[] = [
    { name: 'Body 1', isDisplay: true, id: 1 },
    { name: 'Body 2', isDisplay: true, id: 2 },
    { name: 'Body 3', isDisplay: true, id: 3 },
    { name: 'Body 4', isDisplay: true, id: 4 },
  ];

  public ngOnInit(): void {
    this.setActiveTab1();
  }

  public setActiveTab1(): void {
    this.isShowBlock1 = true;
    this.buttons[0].isActive = true;
  }

  public changeItem(bittonId: number): void {
    this.setActiveTab(bittonId);
    this.buttonId = bittonId;
  }

  private setActiveTab(tabId: number): void {
    this.buttons.forEach((button) => button.id === tabId ? button.isActive = true : button.isActive = false);
  }
}