import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HeaderAppComponent } from "./shared/components/header-app/header-app.component";
import { SliderAppComponent } from "./shared/components/slider-app/slider-app.component";

@NgModule({
  declarations: [
    AppComponent,
    SliderAppComponent,
    HeaderAppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}